import React from "react";
import { useState } from "react";
import "./App.css";

export const ToDoInput = (props) => {
  const [text, setText] = useState("");
  const { setItems, id, setId, setFinishedItems } = props;

  const deleteItem = (id) => {
    // normally, we should only have to call one of these two functions
    // but it won't hurt to do it like this for now
    setItems((prevState) => {
      return prevState.filter((item) => item.id !== id);
    });

    setFinishedItems((prevState) => {
      return prevState.filter((item) => item.id !== id);
    });
  };

  const toggleComplete = (id) => {
    setItems((prevState) => {
      // check our items
      let newFinishedItem = prevState.find((item) => item.id === id);
      if (newFinishedItem) {
        // if it is, add it to the finished items
        setFinishedItems((oldState) => [
          ...oldState.filter((item) => item !== newFinishedItem),
          // I am having issues setting the value of completed to false when moving this to the finished items
          newFinishedItem,
        ]);
        return prevState.filter((item) => item.id !== id);
      } else {
        setFinishedItems((oldState) => {
          // check our finished items for this item
          let newItem = oldState.find((item) => item.id === id);
          if (newItem) {
            // if it is, add it to the items
            setItems((oldState) => [
              ...oldState.filter((item) => item !== newItem),
              newItem,
            ]);
            return oldState.filter((item) => item.id !== id);
          }
          return oldState;
        });
      }
      return prevState;
    });
  };

  const onSubmit = () => {
    setItems((prevState) => [
      ...prevState,
      {
        id,
        text,
        completed: false,
        toggleComplete,
        deleteItem,
      },
    ]);
    setText("");
    setId(id + 1);
  };

  const handleSubmit = () => {
    if (text) {
      if (props.onSubmit) {
        props.onSubmit();
      } else {
        onSubmit();
      }
    }
  };

  return (
    <div className="ToDoInput">
      <input
        type="text"
        value={text}
        placeholder="Enter a new to-do item"
        onChange={(e) => setText(e.target.value)}
        onSubmit={handleSubmit} // the text is not submitted when the "ENTER" key is pressed. why?
      />
      <button onClick={handleSubmit}>Add</button>
    </div>
  );
};
