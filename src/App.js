import "./App.css";
import React from "react";
import { Divider } from "./Divider";
import { ToDoInput } from "./ToDoInput";
import { ToDoListItem } from "./ToDoListItem";
import { useState } from "react";
import { useLoaderData } from "react-router-dom";

const App = () => {
  let [id, setId] = useState(1);
  let [items, setItems] = useState([]);
  let [finishedItems, setFinishedItems] = useState([]);
  let loaderData = useLoaderData();

  return (
    <div className="App">
      <header className="App-header">
        <h3>To-do app</h3>
        <h3>{loaderData}</h3>
      </header>
      <ToDoInput
        setItems={setItems}
        id={id}
        setId={setId}
        items={items}
        setFinishedItems={setFinishedItems}
        finishedItems={finishedItems}
      />

      <div className="ToDoList">
        {!items.length ? (
          <p>You have not added any items to this list</p>
        ) : (
          items.map((item) => (
            <ToDoListItem
              key={item.id}
              item={item}
              toggleComplete={item.toggleComplete}
              deleteItem={item.deleteItem}
            />
          ))
        )}

        <Divider />

        {finishedItems.map((item) => (
          <ToDoListItem
            key={item.id}
            item={item}
            toggleComplete={item.toggleComplete}
            deleteItem={item.deleteItem}
          />
        ))}
      </div>
    </div>
  );
};

export default App;
