export const ToDoListItem = (props) => {
  const { item, toggleComplete, deleteItem } = props;
  return (
    <div className="ToDoListItem">
      <div style={{display: "inline"}}>
        <input
          type="checkbox"
          checked={item.completed}
          onChange={() => toggleComplete(item.id)}
        />
        <span
          style={{ textDecoration: item.completed ? "line-through" : "none" }}
        >
          {item.text}
        </span>
      </div>
      <button onClick={() => deleteItem(item.id)}>X</button>
    </div>
  );
};
