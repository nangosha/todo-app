/**
 * @jest-environment jsdom
 */

import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { ToDoInput } from "../src/ToDoInput";

it("Should render the [Add] button", () => {
  render(<ToDoInput />);
  const addButton = screen.getByText("Add");
  expect(addButton).toBeInTheDocument();
});

it("clicking Add with empty input does nothing", async () => {
  const onSubmitMock = jest.fn();
  render(<ToDoInput onSubmit={onSubmitMock} />);

  const addButton = screen.getByText("Add");

  fireEvent.click(addButton);

  expect(onSubmitMock).toHaveBeenCalledTimes(0);
});

it("clicking Add with non-empty input should call onSubmit", async () => {
  const onSubmitMock = jest.fn();
  render(<ToDoInput onSubmit={onSubmitMock} />);

  const addButton = screen.getByText("Add");
  const inputField = screen.getByPlaceholderText("Enter a new to-do item");

  fireEvent.change(inputField, { target: { value: "Buy milk" } });
  fireEvent.click(addButton);

  expect(onSubmitMock).toHaveBeenCalledTimes(1);
});
